package org.uevora.es.RESTFuldemo;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findById(long id);

    Iterable<User> findAll();

    User save(User user);

    Iterable<User> findByName(String name);

    void deleteById(long id);

}
