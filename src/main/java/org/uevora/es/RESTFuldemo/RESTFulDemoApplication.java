package org.uevora.es.RESTFuldemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RESTFulDemoApplication {

	private static final Logger log = LoggerFactory.getLogger(RESTFulDemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(RESTFulDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(UserRepository repository) {
		return (args) -> {
			// save a few users
			repository.save(new User("dada@teste.com", "author 1"));
			repository.save(new User("dada2@teste.com", "author 2"));

			// fetch all users
			log.info("User found with findAll():");
			log.info("-------------------------------");
			for (User user : repository.findAll()) {
				log.info(user.toString());
			}
			log.info("");

			// fetch an individual user by ID
			User user = repository.findById(2);
			log.info("Customer found with findById(2):");
			log.info("--------------------------------");
			log.info(user.toString());
			log.info("");

			// fetch customers by last name
			log.info("User found with findByName('author 1'):");
			log.info("--------------------------------------------");
			repository.findByName("author 1").forEach(currUser -> {
				log.info(currUser.toString());
			});

			log.info("");
		};
	}

}
