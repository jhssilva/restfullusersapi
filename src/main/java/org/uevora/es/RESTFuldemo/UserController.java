package org.uevora.es.RESTFuldemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class UserController {

    private Auxiliar auxiliar = new Auxiliar();

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/user")
    @ResponseBody
    String createUser(@RequestParam String userEmail, @RequestParam String email, @RequestParam String name) {
        if (auxiliar.hasPermissions(userEmail) == false) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    String.format("User with email=%s not authorized", userEmail));
        }
        User newUser;
        try {
            newUser = new User(email, name);
            userRepository.save(newUser);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    String.format("Error Creating new User: %s", e.getMessage()) + "");
        }

        return "Created with Success!" + newUser.toString();
    }

    @GetMapping("/users")
    @ResponseBody
    Iterable<User> getUsers() {
        Iterable<User> users;
        try {
            users = userRepository.findAll();
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    String.format("Error Getting Users: %s", e.getMessage()) + "");
        }
        return users;
    }

    @GetMapping("/user/{id}")
    User getUser(@PathVariable Long id) {
        return userRepository.findById(id).orElseThrow(() -> {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("User with id=%d not found", id));
        });
    }

    @PutMapping("/user/{id}")
    User updateUser(@RequestParam String userEmail, @RequestBody User newUser, @PathVariable Long id) {
        if (auxiliar.hasPermissions(userEmail) == false) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    String.format("User with email=%s not authorized", userEmail));
        }

        return userRepository.findById(id).map(user -> {
            newUser.setId(id);
            user.setUser(newUser);
            return userRepository.save(user);
        }).orElseGet(() -> {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("User with id=%d not found", id));
        });
    }

    @DeleteMapping("/user/{id}")
    String deleteUser(@RequestParam String userEmail, @PathVariable Long id) {
        // Isn't necessary to verify the email on the requirements,
        // however, would make sense to add a verification
        if (auxiliar.hasPermissions(userEmail) == false) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    String.format("User with email=%s not authorized", userEmail));
        }
        try {
            userRepository.deleteById(id);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    String.format("User with id=%d not found", id));
        }
        return "Deleted with Success! User with id=" + id;
    }

}
