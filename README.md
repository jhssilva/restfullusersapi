# Serviço RESTful

## Introdução

O presente trabalho têm como propósito a criação de um serviço RESTful que será utilizado por outro sistema para fazer a gestão de utilizadores.
O presente trabalho foi realizado em <b>Java Spring</b>. A base de dados utilizada é a <b>H2Dialect</b>.

## Operações

A presente API possui as seguintes operações:

- Criar utilizadores;
- Listar todos os utilizadores;
- Listar os dados de um utilizador;
- Actualizar os dados de um utilizador;
- Apagar os utilizadores.

### API

<details>
 <summary><code>POST</code> <code><b>/user</b></code> <code>Criar utilizadores</code></summary>

##### Parameters

> | name      | type     | data type | description                         |
> | --------- | -------- | --------- | ----------------------------------- |
> | userEmail | required | String    | Email that is performing the action |
> | email     | required | String    | Email to create the new user        |
> | name      | required | String    | Name for the                        |

##### Responses

> | http code | content-type               | response                                                                                                                                                                                                 |
> | --------- | -------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
> | `200`     | `text/plain;charset=UTF-8` | `Created with Success![User[id=50, name='teste@gmail.com', email='teste']]`                                                                                                                              |
> | `400`     | `application/json`         | `{"timestamp": "2023-04-16T12:09:52.370+00:00","code":"400","error":"Bad Request","message": "Required request parameter 'userEmail' for method parameter type String is not present","path": "/user/"}` |
> | `401`     | `application/json`         | `{"timestamp": "2023-04-16T12:09:52.370+00:00","code":"401","error":"Unauthorized","message": "User with email=testes@gmail.com not authorized","path": "/user/"}`                                       |

##### Example cURL

> ```javascript
> curl --location --request POST 'localhost:8080/user/?userEmail=teste%40gmail.com&email=teste&name=teste%40gmail.com'
> ```

</details>

<details>
 <summary><code>GET</code> <code><b>/users</b></code> <code>Listar todos os utilizadores</code></summary>

##### Parameters

> None

##### Responses

> | http code | content-type       | response                                                                                            |
> | --------- | ------------------ | --------------------------------------------------------------------------------------------------- |
> | `200`     | `application/json` | `[{"id": 3,"email": "title 1","name": "author 1"},{"id": 6,"email": "title 2","name": "author 2"}]` |

##### Example cURL

> ```javascript
> curl --location 'localhost:8080/users'
> ```

</details>

<details>
 <summary><code>GET</code> <code><b>/user/{id}</b></code> <code>Listar os dados de um utilizador</code></summary>

##### Parameters

> | name | type     | data type | description      |
> | ---- | -------- | --------- | ---------------- |
> | id   | required | Long      | Id from the user |

##### Responses

> | http code | content-type       | response                                                                             |
> | --------- | ------------------ | ------------------------------------------------------------------------------------ |
> | `200`     | `application/json` | `{"id": 3,"email": "title 1","name":"author1"}`                                      |
> | `404`     | `application/json` | `{"error": "Not Found",  "message": "User with id=1 not found",  "path": "/user/1"}` |

##### Example cURL

> ```javascript
> curl --location 'localhost:8080/user/3'
> ```

</details>

<details>
 <summary><code>PUT</code> <code><b>/user/{id}</b></code> <code>Actualizar os dados de um utilizador</code></summary>

##### Parameters

> | set    | name      | type     | data type | description                         |
> | ------ | --------- | -------- | --------- | ----------------------------------- |
> | params | id        | required | Long      | Id from the user                    |
> | params | userEmail | required | String    | Email that is performing the action |
> | body   | user      | required | json      | New Object User                     |

##### Responses

> | http code | content-type       | response                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
> | --------- | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
> | `200`     | `application/json` | `                                                                                                                                                                                                                                                                                                                                                                                                                                   {"id": 20,"email": "newEmail","name": "newName"}` |
> | `400`     | `application/json` | `{"timestamp": "2023-04-17T18:26:36.771+00:00","status": 400,"error": "Bad Request","message": "JSON parse error: Unexpected character (' (code 125)): was expecting double-quote to starfield name; nested exception is com.fasterxml.jacksocore.JsonParseException: Unexpected character ('}(code 125)): was expecting double-quote to start fielname\n at [Source: (PushbackInputStream); line: 4column: 2]","path": "/user/20"}`                                                  |
> | `404`     | `application/json` | `{"error": "Not Found",  "message": "User with id=1 not found",  "path": "/user/1"}`                                                                                                                                                                                                                                                                                                                                                                                                  |

##### Example cURL

> ```javascript
> curl --location --request PUT 'localhost:8080/user/20?userEmail=teste%40gmail.com' \
> --header 'Content-Type: application/json' \
> --data '{"name": "newName","email": "newEmail"}'
> ```

</details>

<details>
 <summary><code>DELETE</code> <code><b>/user/{id}</b></code> <code>Apagar utilizadores</code></summary>

##### Parameters

> | name      | type     | data type | description                         |
> | --------- | -------- | --------- | ----------------------------------- |
> | id        | required | Long      | Id from the user                    |
> | userEmail | required | String    | Email that is performing the action |

##### Responses

> | http code | content-type               | response                                                                                                                                                           |
> | --------- | -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
> | `200`     | `text/plain;charset=UTF-8` | `Deleted with Success! User with id=20`                                                                                                                            |
> | `401`     | `application/json`         | `{"timestamp": "2023-04-16T12:09:52.370+00:00","code":"401","error":"Unauthorized","message": "User with email=testes@gmail.com not authorized","path": "/user/"}` |
> | `404`     | `application/json`         | `{"timestamp": "2023-04-17T18:36:51.210+00:00","status": 404,"error": "Not Found","message": "User with id=20 not found","path": "/user/20"}`                      |

##### Example cURL

> ```javascript
> curl --location --request DELETE 'localhost:8080/user/20?userEmail=teste%40gmail.com'
> ```

</details>

## Executar Projecto

O projecto pode ser executado de duas formas, sendo a mais recomendada com o Docker.

- Sem Docker

```console
$ mvn spring-boot:run
```

- Com o Docker

```console
$ docker compose up -d
```

## Docker

O Docker é uma tecnologia de virtualização de containers que permite encapsular uma aplicação e todas as suas dependências em um único pacote portátil

A aplicação executada com o Docker permite aumentar a compatibilidade e manutenção do projecto, simplificar e acelerar a configuração e execução deste mesmo.
O Docker permite então partilhar a configuração com outros programadores, criando uma enorme facilidade de execução do projecto em diferentes máquinas, eliminando a necessidade de configurar manualmente cada máquina em que a aplicação será executada.
O Docker isola as aplicaçãoes em containers. Esta capacidade de isolar a aplicação num container, permite evitar conflitos com outro software e bibliotecas instaladas na máquina. O que permite garantir que a aplicação funcione consistente em diferentes ambientes, evitando o conflito entre dependências.

A configuração do Docker é feita no ficheiro **docker-compose.yml**.

```console
services:
  app:
    image: maven:3.9.1-eclipse-temurin-11-alpine
    command: mvn spring-boot:run
    ports:
      - 8080:8080
    working_dir: /app
    volumes:
      - ./:/app
```

O ficheiro **docker-compose.yml** possui os serviços. Sendo a **app** um dos serviços descritos e o único, que têm como imagem **maven:3.9.1-eclipse-temurin-11-alpine**, e executa o comando **mvn spring-boot:run** após a imagem ser instalada, este comando executará a aplicação. O ports mapeia as portas do projecto para a porta **8080**, onde o projecto será acedido. A instrução working_dir define a directoria de trabalho, onde a aplicação será executada e os restantes comandos do docker serão executados. A instrução volumes cria o volume que definirá onde a informação será guardada depois do docker parar de executar.

## CI/CD

"Continuous Integration e Continuous Delivery ou Continuous Deployment (CI/CD) é um conceito essencial no processo de desenvolvimento de software, que permite às equipas de desenvolvimento agilizar ao máximo todo o processo de testar, construir e disponibilizar o software para utilização por parte do utilizador final, de forma completamente automática."

Para a implementação do CI/CD foi criado um ficheiro **_.gitlab-ci.yml_** na root do projecto. Este ficheiro possui a configuração do CI/CD que o gitlab utilizará.

O ficheiro possui a seguinte configuração:

```console
variables:
  CONTAINER_DEFAULT_IMAGE: "maven:3.9.1-eclipse-temurin-11-alpine"
  CONTAINER_REGISTER_IMAGE_VERSION: "1.0.0"
  BRANCH: "master"

build-project:
  rules:
    - if: $CI_COMMIT_BRANCH == $BRANCH
  stage: build
  image: maven:latest
  script:
    - echo "Hello, $GITLAB_USER_LOGIN!"
    - echo "This job builds the project from the $CI_COMMIT_BRANCH branch."
    - mvn -Dmaven.test.skip=true package

create-and-publish-docker-image:
  rules:
    - if: $CI_COMMIT_BRANCH == $BRANCH
  stage: deploy
  image: docker:latest
  services:
    - docker:latest
  script:
    - echo "This job creates and publishes a docker image."
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD
    - docker compose up --build -d
    - docker tag $CONTAINER_DEFAULT_IMAGE $CONTAINER_REGISTER_IMAGE:$CONTAINER_REGISTER_IMAGE_VERSION
    - docker push $CONTAINER_REGISTER_IMAGE:$CONTAINER_REGISTER_IMAGE_VERSION
```

Foi criado 2 jobs/tarefas, que executam as seguintes ações:

- Build do projecto;
- Construir a imagem docker;
- Publicar a imagem docker num Docker Registry.

Em ambas as tarefas é verificado se o branch que fez commit é o **master** branch. (No entanto, este parametro é configurável, nas varíaveis.)

A primeira tarefa _build-project_, é executada no stage _build_ e utiliza como imagem _maven:latest_. Foi utilizada esta imagem, devido ao facto de necessitar de executar o comando **mvn**.
O script executa 3 instruções, sendo que as primeiras imprimem uma mensagem para a consola. A terceira faz build do projecto.

Na segunda tarefa **create-and-publish-docker-image** é criada e publicada a imagem do docker. Foi optado por utilizar a mesma tarefa visto, que para publicar é necessário ter um build da imagem.
É utilizado o stage _deploy_, este stage foi escolhido porque faria deploy da imagem para o Docker Registry. A imagem utilizada foi a _docker:latest_ isto devido a necessidade de utilizar as componentes do docker e não estar disponivel na imagem predefinida. Foi adicionado o _services_ por um conflito/erro que surgiu na execução da tarefa.
O script executa 5 ações, nas quais foi feito o login no docker para poder publicar a imagem no Docker Registry. Foi compilado a imagem, e foi escolhido o comando **docker compose up --build -d**, devido ao facto, que para fazer **docker compose build** era necessário criar um DockerFile e alterar o _docker-compose.yml_ adicionando o path para o **build**. Então por questões de simplicidade, foi utilizado o comando mencionado anteriormente. Depois é executado o comando Tag para renomear a tag e por fim publicar a imagem para o Docker Repository.

Em relação ás varíaveis, existem váriaveis que podem ser alteradas no ficheiro **.gitlab-ci.yml**, enquanto outras são alteradas e configuráveis na plataforma gitlab.
As variaveis configuraveis na plataforma gitlab são variaveis como o nome do utilizador (CI_REGISTRY_USER), palavra-passe (CI_REGISTRY_PASSWORD) e o nome da Imagem no Docker Registry (CONTAINER_REGISTER_IMAGE).
As váriáveis configuraveis no ficheiro são o nome do branch (BRANCH), o nome da imagem construida anteriormente (CONTAINER_DEFAULT_IMAGE) e a versão a ser publicada no Docker Registry (CONTAINER_REGISTER_IMAGE_VERSION).

## Autor

- Hugo Silva (m53080@alunos.uevora.pt)
